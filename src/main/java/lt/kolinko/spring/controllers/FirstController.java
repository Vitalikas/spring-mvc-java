package lt.kolinko.spring.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cmd")
public class FirstController {
    @GetMapping("/hello")
    public String sayHello() {
        return "first/hello";
    }

    @GetMapping("/bye")
    public String sayBye() {
        return "first/bye";
    }
}
