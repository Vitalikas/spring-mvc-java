package lt.kolinko.spring.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/first")
public class CalculatorController {
    @GetMapping("/calculator")
    public String calculate(@RequestParam("a") double a,
                            @RequestParam("b") double b,
                            @RequestParam("action") String action,
                            Model model) {
        double result;

        switch (action) {
            case "multiplication":
                result = a * b;
                System.out.println("Multiplication: " + result);
                break;
            case "addition":
                result = a + b;
                System.out.println("Addition: " + result);
                break;
            case "subtraction":
                result = a - b;
                System.out.println("Subtraction: " + result);
                break;
            case "division":
                result = a / b;
                System.out.println("Division: " + result);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + action);
        }

        model.addAttribute("result", result);

        return "calc/calc";
    }
}
