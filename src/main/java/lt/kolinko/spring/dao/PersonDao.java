package lt.kolinko.spring.dao;

import lt.kolinko.spring.models.Person;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class PersonDao {
    private final List<Person> people;
    int  index;

    {
        people = new ArrayList<>();

        people.addAll(Arrays.asList(
                new Person(++index, "Mike"),
                new Person(++index, "Nick"),
                new Person(++index, "Tom")));
    }

    public List<Person> getPeople() {
        return people;
    }

    public Person getPersonById(int id) {
        return people.stream()
                .filter(person -> person.getId() == id)
                .findAny()
                .orElse(null);
    }

    public void save(Person person) {
        person.setId(++index);
        people.add(person);
    }

    public void update(Person personFromTh, int id) {
        Person personToBeUpdated = getPersonById(id);
        personToBeUpdated.setName(personFromTh.getName());
    }

    public void delete(int id) {
        people.removeIf(person -> person.getId() == id);
    }
}
